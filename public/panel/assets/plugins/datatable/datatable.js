$(function(e) {
    $('#users-table').DataTable({
        "language": {
            "lengthMenu": "Sayfada  _MENU_ adet listele",
            "zeroRecords": "Üzgünüm, bulunamadı.",
            "info": "Sayfa _PAGE_",
            "infoEmpty": "Aradığınız kayıt bulunamadı",
            "infoFiltered": "( _MAX_ Adet Kayıt Arasından Filtrelendi)",
            "paginate": {
                "first":      "İlk",
                "last":       "Son",
                "next":       "Sonraki",
                "previous":   "Önceki"
            },
            "search": "Ara",
            "searchable": true
        }

    } );

    $('#blogs-table').DataTable({
        "language": {
            "lengthMenu": "Sayfada  _MENU_ adet listele",
            "zeroRecords": "Üzgünüm, bulunamadı.",
            "info": "Sayfa _PAGE_",
            "infoEmpty": "Aradığınız blog bulunamadı",
            "infoFiltered": "( _MAX_ Adet Blog Arasından Filtrelendi)",
            "paginate": {
                "first":      "İlk",
                "last":       "Son",
                "next":       "Sonraki",
                "previous":   "Önceki"
            },
            "search": "Ara",
            "searchable": true
        }

    } );
    $('#sliders-table').DataTable({
        "language": {
            "lengthMenu": "Sayfada  _MENU_ adet listele",
            "zeroRecords": "Üzgünüm, bulunamadı.",
            "info": "Sayfa _PAGE_",
            "infoEmpty": "Aradığınız slider bulunamadı",
            "infoFiltered": "( _MAX_ Adet Slider Arasından Filtrelendi)",
            "paginate": {
                "first":      "İlk",
                "last":       "Son",
                "next":       "Sonraki",
                "previous":   "Önceki"
            },
            "search": "Ara",
        }

    } );
} );
