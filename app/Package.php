<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

    protected $fillable = ["title", "slug", "image", "description", "price", "discount_price", "rank", "category_id", "user_id", "created_at", "updated_at"];

    public function products(){
        return $this->belongsToMany(Product::class, "package_products", "package_id","product_id");
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class, "category_id");
    }

    public function owner(){
        return $this->hasOne(User::class);
    }
}
