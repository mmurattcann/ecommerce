<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ["title","image","is_cover","product_id"];

    public function product(){
        $this->belongsTo(Product::class,"product_id");
    }
}
