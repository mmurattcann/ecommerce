<?php

function frontView($view = ""){

    $frontViewPath = "front.furniture_theme.".$view;

    return view($frontViewPath);
}

function frontAsset($asset = "") {

    $frontAssetPath = "/frontend/assets/".$asset;

    return asset($frontAssetPath);
}

function isActiveMenu($route){
    if(request()->is($route))
        return true;
    else
        return false;
}

function getLogo($image = null){

    if($image == null) return url("uploads/logo/logo.png");

    return url("storage/uploads/options/logo/".$image);
}

function frontImage($module, $image){
    return url("storage/uploads/{$module}/{$image}");
}
function getFavicon($image){
    if($image != null) return url("storage/uploads/options/favicon/".$image);

    return "Tanımlı Değil";
}

function turkishDate($date){
    $tarih = date("d F Y", strtotime($date));

    // Çıktı: 23 June Tuesday

    $ing_aylar = array("January","February","March","May","April","June","July","August","September","October","November","December");
    $tr_aylar = array("Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık");

    $ing_gunler = array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");
    $tr_gunler = array("Pazartesi","Salı","Çarşamba","Perşembe","Cuma","Cumartesi","Pazar");

    $tarih = str_replace($ing_aylar,$tr_aylar,$tarih);
    $tarih = str_replace($ing_gunler,$tr_gunler,$tarih);

    return  $tarih;
    // Çıktı: 23 Haziran Salı
}
