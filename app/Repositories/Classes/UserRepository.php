<?php


namespace App\Repositories\Classes;


use App\Enums\UserEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Repositories\Interfaces\IUserRepository;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Murattcann\LaraImage\LaraImage;

class UserRepository implements IUserRepository
{
    private $user     = null;
    private $userEnum = null;
    public function __construct()
    {
        $this->user = new User();
        $this->userEnum = new UserEnums();
    }

    public function baseQuery()
    {
        return $this->user::query();
    }

    public function getById(Int $id)
    {
        return  $this->baseQuery()->find($id);
    }

    public function getAll($orderBy = ["id", "asc"])
    {
        return $this->baseQuery()->orderBy($orderBy[0],$orderBy[1])->get();
    }

    public function store(Request $request)
    {
        $image = $request->file("image");

        $data = $request->except("password_confirmation");
        $data["image"] = LaraImage::upload("store","users/profile-pictures",$request->get("name"),$image,"100","100");
        return $this->user->create($data);
    }

    public function update($id, Request $request)
    {
        $user     = $this->getById($id);
        $isActive = $user->is_active == $this->userEnum::_ACTIVE_USER ? $this->userEnum::_ACTIVE_USER : $this->userEnum::_INACTIVE_USER;

        if($request->has("password")){

            if($request->get("password") != $request->get("password_confirmation"))
            {

                return RedirectHelper::RedirectWithErrorFlashMessage("password");
            }
            else{
                $passwordedData = [
                    "name" => $request->name,
                    "email" => $request->email,
                    "phone" => $request->phone,
                    "about" => $request->about,
                    "role" => $request->role,
                    "is_active" => $isActive,
                    "password" => Hash::make($request->password)
                ];
                if($request->file("image")){

                    $image = $request->file("image");
                    $passwordedData["image"] = LaraImage::upload("update","uploads/users/profile-pictures/",$passwordedData["name"], $image, 100, 100);
                }
                $this->user->where("id",$id)->update($passwordedData);
            }
        }

        $data = $request->except(["password", "password_confirmation","profilePictureRadio","_token", "_method"]);

        if($request->file("image")){

            $image = $request->file("image");
            $data["is_active"] = $isActive;
            $data["image"] = LaraImage::upload("update","uploads/users/profile-pictures/",$data["name"], $image, 100, 100);
        }

       return $user;
    }
    public function updateUserStatus(Request $request)
    {
        $id = $request->id;

        $user = $this->getById($id);

        $active = $this->userEnum::_ACTIVE_USER;

        $inActive = $this->userEnum::_INACTIVE_USER;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $user->is_active = $is_active;

        return $user->save();
    }

    public function delete($id)
    {
        $user = $this->getById($id);

        LaraImage::deleteUploadedFile("users/profile-pictures", $user->image);

        $user->delete();

        RedirectHelper::RedirectWithSuccessFlashMessage("destroy","users.index");
    }
}
