<?php


namespace App\Repositories\Classes\ExtendedClasses;


use App\Repositories\Classes\ProductRepository;

class ProductRepositoryExtended extends ProductRepository
{

    public function filterByTitle($title){
        return $this->listingMap($this->baseQuery()
            ->active()
            ->where("title", "like", "%$title%")
            ->get());
    }

    public function filterByCategory($categoryID)
    {

        return $this->listingMap($products = $this->baseQuery()
            ->active()
            ->where("category_id",$categoryID)
            ->get());

    }

    public function filterByUniversity($universityID)
    {

        return $this->listingMap($this->baseQuery()
            ->active()
            ->where("university_id",$universityID)
            ->get());

    }

    public function filterByFaculty($facultyID)
    {

        return $this->listingMap($this->baseQuery()
            ->active()
            ->where("faculty_id",$facultyID)
            ->get());

    }

    public function filterByLowestPrice($minPrice)
    {

        return $this->listingMap($this->baseQuery()
            ->active()
            ->where("price", ">=",$minPrice)
            ->get());
    }

    public function filterByHighestPrice($maxPrice)
    {

        return $this->listingMap($products = $this->baseQuery()
            ->active()
            ->where("price", "<=",$maxPrice)
            ->get());
    }
}
