<?php
namespace App\Repositories\Classes;

use App\BlogCategory;
use App\Enums\BlogEnums;
use App\Repositories\Interfaces\IBaseRepository;
use App\Repositories\Interfaces\IBlogCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogCategoryRepository implements IBaseRepository
{
    protected $model;
    protected $enum;

    public function __construct()
    {
        $this->model = new BlogCategory();
        $this->enum  = new BlogEnums();
    }

    public function baseQuery()
    {
        return $this->model::query();
    }

    public function getById(int $id)
    {
        return $this->baseQuery()->find($id);
    }

    public function getBySlug(string $slug)
    {
        return $this->baseQuery()->with("blogs")->where('slug', $slug)->get();
    }

    public function getAll(string $order = "id", string $by = "asc")
    {
        return $this->baseQuery()->orderBy($order, $by)->get();
    }

    public function getAllWithPaginate(int $limit = 10)
    {

        return $this->baseQuery()->paginate($limit);
    }

    public function getAllWithWhere( string $where = null , string $condition = null)
    {

        return $this->baseQuery()->where($where, $condition)->get();
    }

    /*
     * if you change a spesific value of request key
     * For Example; if you want to modify file request named image
     * You can use like that:
     *
     * $data["image"] = Mmurattcann/LaraImage::upload($request->file('image'))
    */
    public function store(Request $request)
    {

        $data = $request->all();

        $slug = $data["slug"];

        if($slug == null || $slug == '')
            $data["slug"] = Str::slug($data["title"],"-");

        return  $this->baseQuery()->create($data);
    }

    /*
     * You can also modify this request
     *  For Example;
     *
     *  $data["name"] = Str::upper("john doe");
     */
    public function update(Request $request, int $id)
    {

        $model = $this->getById($id);

        $data = $request->all();

        return $model->update($data);
    }
    public function updateStatus(Request $request)
    {
        $id = $request->id;

        $page = $this->getById($id);

        $active = $this->enum::_ACTIVE;

        $inActive = $this->enum::_INACTIVE;

        $is_active = $request->get("is_active") == $active ? $active : $inActive;

        $page->is_active = $is_active;

        return $page->save();
    }
    public function destroy(int $id)
    {

        $model = $this->getById($id);

        return $model->delete();
    }
}
