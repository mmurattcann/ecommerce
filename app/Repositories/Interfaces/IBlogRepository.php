<?php
namespace App\Repositories\Interfaces;

use App\Http\Requests\Blog\BlogStoreRequest;
use App\Http\Requests\Blog\BlogUpdateRequest;
use Illuminate\Http\Request;

interface IBlogRepository
{

    public function getById(int $id);

    public function getBySlug(string $slug);

    public function getAll(string $order = "id", string $by = "asc");

    public function getAllWithPaginate(int $limit = 10);

    public function getAllWithWhere( string $where = null, string $operator, $condition = null);

    public function store(BlogStoreRequest $request);

    public function update(BlogUpdateRequest $request, int $id);

    public function updateBlogStatus(Request $request);

    public function destroy(int $id);
}
