<?php


namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

interface IOptionRepository
{
    public function baseQuery();

    public function getById(int $id);

    public function update(Request $request, int $id);

    public function getFirst();
}
