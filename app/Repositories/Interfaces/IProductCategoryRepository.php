<?php


namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

interface IProductCategoryRepository
{
    public function baseQuery();

    public function getById(Int $id);

    public function getAll($orderBy = ["id", "asc"]);

    public function getAllParents($orderBy = ["id", "asc"]);

    public function getAllChilds($orderBy = ["id", "asc"]);

    public function store(Request $request);

    public function update($id, Request $request);

    public function delete($id);

    public function updateCategoryStatus(Request $request);

}
