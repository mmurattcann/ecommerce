<?php


namespace App\Enums;


class PageEnums
{
    const _ABOUT_US = 0;
    const _FAQS     = 1;
    const _MISSION  = 2;
    const _VISION   = 3;

    const _ACTIVE_PAGE   = 1;
    const _INACTIVE_PAGE = 0;

    const _ACTIVE_PAGE_STRING   = "Aktif";
    const _INACTIVE_PAGE_STRING = "Pasif";

}
