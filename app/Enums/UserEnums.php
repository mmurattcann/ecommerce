<?php


namespace App\Enums;


class UserEnums
{
    const _ADMIN_USER    = 1;
    const _STANDARD_USER = 0;

    const _ADMIN_USER_STRING    = "Admin";
    const _STANDARD_USER_STRING = "Standart Kullanıcı";

    const _ACTIVE_USER   = 1;
    const _INACTIVE_USER = 0;

    const _ACTIVE_USER_STRING = "Aktif";
    const _INACTIVE_USER_STRING = "Pasif";
}
