<?php


namespace App\Enums;


class BlogEnums
{

    const _ACTIVE    = 1;
    const _INACTIVE  = 0;

    const _ACTIVE_STRING   = "Aktif";
    const _INACTIVE_STRING = "Pasif";
}
