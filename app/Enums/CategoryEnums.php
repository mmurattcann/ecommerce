<?php


namespace App\Enums;


class CategoryEnums
{
    const _ACTIVE_CATEGORY   = 1;
    const _INACTIVE_CATEGORY = 0;

    const _PARENT_CATEGORY_STRING = "Ana Kategori";
    const _CHILD_CATEGORY_STRING  = "Alt Kategori";

    const _ACTIVE_CATEGORY_STRING   = "Aktif";
    const _INACTIVE_CATEGORY_STRING = "Pasif";

}
