<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $guarded = ["id"];
    protected $fillable = [
        "top_title",
        "title",
        "description",
        "image",
        "rank",
        "is_active",
        "has_button",
        "url",
        "button_title",
        "button_class"
    ];

    public function getButtonStatusAttribute($value){
        return $this->has_button == 1 ? "Var": "Yok";
    }
}
