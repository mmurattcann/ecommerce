<?php

namespace App;

use App\Enums\UserEnums;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id"];
    protected $fillable = [
        'name', 'email', 'password','phone', 'about', 'image', 'role', 'is_active', 'verify_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function scopeActive($query){
        return $query->where("is_active", UserEnums::_ACTIVE_USER);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function blogs(){
        return $this->hasMany(Blog::class);
    }
}
