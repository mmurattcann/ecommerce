<?php

namespace App\Providers;

use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductImageRepository;
use App\Repositories\Classes\ProductRepository;
use App\Repositories\Classes\UserRepository;
use App\Repositories\Interfaces\IProductCategoryRepository;
use App\Repositories\Interfaces\IProductImageRepository;
use App\Repositories\Interfaces\IProductRepository;
use App\Repositories\Interfaces\IUserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(IUserRepository::class, UserRepository::class);
        $this->app->singleton(IProductCategoryRepository::class, ProductCategoryRepository::class);
        $this->app->singleton(IProductRepository::class, ProductRepository::class);
        $this->app->singleton(IProductImageRepository::class, ProductImageRepository::class);
    }
}
