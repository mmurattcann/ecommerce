<?php

namespace App\Providers;

use App\Repositories\Classes\BlogCategoryRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\OptionRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);

        if(!$this->app->runningInConsole()){

            $optionRepository = new  OptionRepository();

            $options = Cache::remember('options', 60, function() use ($optionRepository)
            {
                return $optionRepository->detailMap($optionRepository->getFirst());
            });

            config()->set('options', $options);

        }

            $productCategoryRepository = new ProductCategoryRepository();
            $blogCategoryRepository = new BlogCategoryRepository();
            $optionRepository = new OptionRepository();


            $data = [
                "productCategories" => $productCategoryRepository->getAllParents(),
                "blogCategories"    => $blogCategoryRepository->getAll(),
                "siteTitle" => config("options.site_title"),
                "siteAddress" => config("options.address"),
                "mobilePhoneNumber" => config("options.gsm"),
                "phoneNumber" => config("options.phone"),
                "contactEmail" => config("options.contact_email"),
                "infoEmail" => config("options.info_email"),
                "siteLogo" => config("options.logo"),
                "favicon" => config("options.favicon"),
                "siteKey" => config("options.site_key"),
                "secretKey" => config("options.secret_key"),
                "googleMap" => config("options.google_map")
            ];



            return View::share($data);
    }
}
