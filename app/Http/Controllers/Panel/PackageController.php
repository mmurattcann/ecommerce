<?php

namespace App\Http\Controllers\Panel;

use App\Enums\ProductEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\PackageRepository;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Classes\ProductImageRepository;
use App\Repositories\Classes\ProductRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    use GeneralCrud, StatusUpdater;
    private $repository         = null;
    private $productRepository       = null;
    private $productImageRepository  = null;
    private $universityRepository    = null;
    private $facultyRepository       = null;
    private $categoryRepository = null;
    private $productEnums       = null;
    private $resource           = "packages";
    public function __construct()
    {
        $this->repository = new PackageRepository();
        $this->productRepository         = new ProductRepository();
        $this->productImageRepository  = new ProductImageRepository();
        $this->categoryRepository = new ProductCategoryRepository();
        $this->productEnums       = new ProductEnums();
    }

    public function index()
    {

        $data = [
            "title" => "Paket Yönetimi",
            "packages" => $this->repository->getAll(),
            "isActive"   => $this->productEnums::_ACTIVE_PRODUCT
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create()
    {
        $data = [
            "title" => "Yeni Paket Kaydı",
            "products" => $this->productRepository->getAllActive(),
            "categories" => $this->categoryRepository->getAll("title", "asc"),
        ];

        return panelView("$this->resource.create")->with($data);
    }

    public function edit($id)
    {
        $package = $this->repository->getById($id);

        $data = [
            "title" => $package->title. " başlıklı paket düzenleniyor",
            "product" => $package,
            "categories" => $this->categoryRepository->getAll(),
            "products" => $this->productRepository->getAllActive(),
        ];
        return  panelView("$this->resource.edit")->with($data);
    }

}
