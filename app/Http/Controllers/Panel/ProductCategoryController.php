<?php

namespace App\Http\Controllers\Panel;

use App\Enums\CategoryEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\ProductCategory;
use App\Repositories\Classes\ProductCategoryRepository;
use App\Repositories\Interfaces\IProductCategoryRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    use GeneralCrud, StatusUpdater;

    private  $repository = null;
    private  $resource   = "product-categories";
    private  $categoryEnum = null;

    public function __construct()
    {
        $this->repository = new ProductCategoryRepository();
        $this->categoryEnum = new CategoryEnums();
    }

    public function index()
    {
        $categories = $this->repository->getAll();

        $data = [
            "title" => "Ürün Kategorisi Yönetimi",
            "categories" => $categories,
            "isActive"  => $this->categoryEnum::_ACTIVE_CATEGORY
        ];

        return panelView("$this->resource.index")->with($data);
    }


    public function create()
    {
        $data = [
            "title" => "Yeni Ürün Kategorisi Kaydı",
            "parentCategories" => $this->repository->getAllParents()
        ];

        return panelView("$this->resource.create")->with($data);
    }


    public function edit($id)
    {
        $category = $this->repository->getById($id);

        $parents  = $this->repository->getAllParents("title","asc");

        $data = [
            "title" => $category->title. " Başlıklı Kategori Düzenleniyor",
            "category" => $category,
            "parentCategories" => $parents,
        ];

        return panelView("$this->resource.edit")->with($data);
    }

}
