<?php

namespace App\Http\Controllers\Panel;

use App\Enums\PageEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\PageRepository;
use Illuminate\Http\Request;

class PageController extends Controller
{
    private $repository = null;
    private $pageEnum   = null;
    private $resource   = null;

    public function __construct()
    {
        $this->repository = new PageRepository();
        $this->pageEnum   = new PageEnums();
        $this->resource   = "pages";
    }
    public function index()
    {
        $data = [
            "title"    => "Sayfa Yönetimi",
            "pages"    => $this->repository->getAll(),
            "isActive" => $this->pageEnum::_ACTIVE_PAGE,
        ] ;

        return panelView("$this->resource.index")->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            "title" => "Yeni Sayfa Oluştur",
        ];

        return panelView("$this->resource.create")->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->repository->store($request);
        return RedirectHelper::RedirectWithSuccessFlashMessage("store","$this->resource.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = $this->repository->getById($id);

        $data = [
            "title" => $page->title .  " Sayfası Düzenleniyor",
            "page"  => $page
        ];

        return panelView("$this->resource.edit")->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repository->update($request,$id);
        return RedirectHelper::RedirectWithSuccessFlashMessage("update", "$this->resource.index");
    }

    public function updatePageStatus(Request $request){
       $this->repository->updatePageStatus($request);
       return response()->json(["message" => "Sayfa Durumu Güncellendi"]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->destroy($id);
        return response()->json([
            "message" => "Kayıt Başarıyla Silindi"
        ]);
    }
}
