<?php

namespace App\Http\Controllers\Panel;

use App\Enums\BlogEnums;
use App\Helpers\RedirectHelper\RedirectHelper;
use App\Http\Controllers\Controller;
use App\Repositories\Classes\SliderRepository;
use App\Traits\GeneralCrud;
use App\Traits\StatusUpdater;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use GeneralCrud, StatusUpdater;

    private $repository = null;
    private $resource   = "sliders";
    private $enum =  null;

    public function __construct()
    {
        $this->repository = new SliderRepository();
        $this->enum = new BlogEnums();
    }

    public function index(){

        $sliders = $this->repository->getAll();
        $data = [
            "title" => "Slider Yönetimi",
            "sliders" => $sliders,
            "isActive" => $this->enum::_ACTIVE,
        ];

        return panelView("$this->resource.index")->with($data);
    }

    public function create(){
        $title = "Yeni Slider Ekle";
        $normalButtonClasses = config("buttonClasses.normalButtons");
        $outlinedButtonClasses = config("buttonClasses.outlineButtons");

        $data = [
            "title" => $title,
            "normalButtonClasses" => $normalButtonClasses,
            "outlinedButtonClasses" => $outlinedButtonClasses,
        ];

        return panelView("$this->resource.create")->with($data);
    }


    public function edit($id){
        $slider = $this->repository->getById($id);
        $normalButtonClasses = config("buttonClasses.normalButtons");
        $outlinedButtonClasses = config("buttonClasses.outlineButtons");
        $data = [
            "title" => $slider->title." başlıklı slider düzenleniyor",
            "slider" => $slider,
            "normalButtonClasses" => $normalButtonClasses,
            "outlinedButtonClasses" => $outlinedButtonClasses,
        ];

        return panelView("$this->resource.edit")->with($data);
    }

}
