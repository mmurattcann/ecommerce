<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Classes\BlogRepository;
use App\Repositories\Classes\SliderRepository;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $sliderRepository = null;
    protected $blogRepository = null;

    public function __construct()
    {
        $this->sliderRepository = new SliderRepository();
        $this->blogRepository   = new BlogRepository();
    }

    public function index(){
        $data = [
            "title" => "Anasayfa",
            "sliders" => $this->sliderRepository->getAllActive(),
            "blogs" => $this->blogRepository->listingMap($this->blogRepository->getIndexBlogs())
        ];

        return frontView("index")->with($data);
    }
}
