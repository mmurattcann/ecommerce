@extends('layouts.panel._authentication_layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col mx-auto">
                <div class="text-center mb-6">
                    <img src="{{panelAsset("images/brand/logo.png")}}" class="" alt="">
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card-group mb-0">
                            <div class="card p-4">
                                <div class="card-body">
                                    <h1>Kaydol</h1>
                                    <p class="text-muted">Forma bilgilerinizi girin</p>
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>

                                            <div class="col-md-12">
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-form-label ">{{ __('E-Mail Address') }}</label>

                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class=" col-form-label ">{{ __('Password') }}</label>

                                            <div class="col-md-12">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password-confirm" class=" col-form-label ">{{ __('Confirm Password') }}</label>

                                            <div class="col-md-12">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Kaydol') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">

                                    </div>
                                </div>

                            </div>
                            <div class="card text-white bg-primary py-5 d-md-down-none login-transparent">
                                <div class="card-body text-center justify-content-center ">
                                    <h2>Giriş</h2>
                                    <p>Formda bulunan alanları doldurarak giriş yapabilir, panele ait özellikleri kullanmaya başlayabilirsiniz.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
