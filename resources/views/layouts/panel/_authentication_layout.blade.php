<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="msapplication-TileColor" content="#ff685c">
    <meta name="theme-color" content="#32cafe">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- Title -->
    <title>Giriş Yap</title>
    <link rel="stylesheet" href="{{panelAsset("fonts/fonts/font-awesome.min.css")}}">

    <!-- Font Family -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

    <!-- Dashboard Css -->
    <link href="{{panelAsset("css/dashboard.css")}}" rel="stylesheet" />


    <!-- Custom scroll bar css-->
    <link href="{{panelAsset("plugins/scroll-bar/jquery.mCustomScrollbar.css")}}" rel="stylesheet" />

    <!---Font icons-->
    <link href="{{panelAsset("plugins/iconfonts/plugin.css")}}" rel="stylesheet" />

</head>
<body class="login-img">
<div class="page">
    <div class="page-single">
        @yield("content")
    </div>
</div>


<!-- Dashboard js -->
<script src="{{panelAsset("js/vendors/jquery-3.2.1.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/bootstrap.bundle.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/jquery.sparkline.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/selectize.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/jquery.tablesorter.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/circle-progress.min.js")}}"></script>
<script src="{{panelAsset("plugins/rating/jquery.rating-stars.js")}}"></script>

<!-- Custom scroll bar Js-->
<script src="{{panelAsset("plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js")}}"></script>

</body>
</html>
