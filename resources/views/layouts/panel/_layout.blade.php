<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#ff685c">
    <meta name="theme-color" content="#32cafe">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="MobileOptimized" content="320">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

    <!-- Title -->
    <title>@yield("title") - Stuket Panel</title>

    <link rel="stylesheet" href="{{panelAsset("fonts/fonts/font-awesome.min.css")}}">

    <!-- Font family -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,700" rel="stylesheet">

    <!-- Dashboard Css -->
    <link href="{{panelAsset("css/dashboard.css")}}" rel="stylesheet" />

    <!-- c3.js Charts Plugin -->
    <link href="{{panelAsset("plugins/charts-c3/c3-chart.css")}}" rel="stylesheet" />

    <!--  Table Plugin -->
    <link href="{{panelAsset("plugins/tables/style.css")}}" rel="stylesheet" />

    <!-- Custom scroll bar css-->
    <link href="{{panelAsset("plugins/scroll-bar/jquery.mCustomScrollbar.css")}}" rel="stylesheet" />

    <!-- Sidemenu Css -->
    <link href="{{panelAsset("plugins/toggle-sidebar/sidemenu.css")}}" rel="stylesheet" />

    <!---Font icons-->
    <link href="{{panelAsset("plugins/iconfonts/plugin.css")}}" rel="stylesheet" />

    <!-- Toastr Js-->
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">


    @stack("css")
</head>
<body class="app sidebar-mini rtl">
<div id="global-loader" ></div>
<div class="page">
    <div class="page-main">
       @include("panel.includes.header")

        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        @include("panel.includes.sidebar")

        <div class="app-content  my-3 my-md-5">
           @yield("content")
        </div>
    </div>
</div>

<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fa fa-angle-up"></i></a>

<!-- Dashboard Core -->
<script src="{{panelAsset("js/vendors/jquery-3.2.1.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/bootstrap.bundle.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/jquery.sparkline.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/selectize.min")}}.js"></script>
<script src="{{panelAsset("js/vendors/jquery.tablesorter.min.js")}}"></script>
<script src="{{panelAsset("js/vendors/circle-progress.min.js")}}"></script>
<script src="{{panelAsset("plugins/rating/jquery.rating-stars.js")}}"></script>
<script src="{{panelAsset("plugins/echarts/echarts.js")}}"></script>

<!-- Fullside-menu Js-->
<script src="{{panelAsset("plugins/toggle-sidebar/sidemenu.js")}}"></script>

<!-- peitychart -->
<script src="{{panelAsset("plugins/peitychart/jquery.peity.min.js")}}"></script>
<script src="{{panelAsset("plugins/peitychart/peitychart.init.js")}}"></script>

<!-- c3.js Charts Plugin -->
<script src="{{panelAsset("plugins/charts-c3/d3.v5.min.js")}}"></script>
<script src="{{panelAsset("plugins/charts-c3/c3-chart.js")}}"></script>

<!-- Input Mask Plugin -->
<script src="{{panelAsset("plugins/input-mask/jquery.mask.min.js")}}"></script>

<!-- Index Scripts -->
<script src="{{panelAsset("js/index.js")}}"></script>
<script src="{{panelAsset("js/index2.js")}}"></script>

<!--Morris.js Charts Plugin -->
<script src="{{panelAsset("plugins/morris/raphael-min.js")}}"></script>
<script src="{{panelAsset("plugins/morris/morris.js")}}"></script>

<!-- Custom scroll bar Js-->
<script src="{{panelAsset("plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js")}}"></script>

<!--  Chart js -->
<script src="{{panelAsset("plugins/chart/Chart.bundle.js")}}"></script>

<!-- Search Js-->
<script src="{{panelAsset("js/prefixfree.min.js")}}"></script>

<!-- Custom Js-->
<script src="{{panelAsset("js/custom.js")}}"></script>

<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>

@stack("js")
{!! Toastr::message() !!}
</body>
</html>
