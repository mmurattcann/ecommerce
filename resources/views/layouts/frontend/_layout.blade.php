@include("front.furniture_theme.includes.head")

<!-- ============================================== HEADER : END ============================================== -->

    @yield("content")
    <!-- /.container -->

<!-- /#top-banner-and-menu -->


@include("front.furniture_theme.includes.footer")
