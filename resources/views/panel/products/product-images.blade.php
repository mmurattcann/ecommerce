@extends("layouts.panel._layout")
@section("title")
    Ürün Görselleri
@endsection
@push("css")
    <!-- DROPZone -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <!-- select2 Plugin -->
    <link href="{{panelAsset("plugins/select2/select2.min.css")}}" rel="stylesheet" />

    <!-- Bootstrap Toggle css -->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- WYSIWYG Editor css -->
    <link href="{{panelAsset("plugins/wysiwyag/richtext.css")}}" rel="stylesheet" />
    <!-- forn-wizard css-->
    <link href="{{panelAsset("plugins/forn-wizard/css/material-bootstrap-wizard.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/forn-wizard/css/demo.css")}}" rel="stylesheet" />

    <!---Sweetalert Css-->
    <link href="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.css")}}" rel="stylesheet" />
    <link href="{{panelAsset("plugins/sweet-alert/sweetalert.css")}}" rel="stylesheet" />
   <style>
       .wizard-ul {
           list-style: none;
           text-align: center;
       }

       .wizard-ul li {
           display: inline-block;
       }
   </style>
@endpush

@section("content")
    <ol class="breadcrumb breadcrumb-arrow mt-3 mb-3">
    </ol>
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"> Ürün Görselleri</div>
                </div>
                <div class="card-body p-6">
                    <div class="row">
                        <div class="col-md-12">

                            <form action="{{route("product-images.store")}}" class="dropzone" id="dropzone" name="image-form" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method("POST")
                                <input type="hidden" name="product_id" value="{{$productID}}">
                                <div class="dz-message">
                                    <div class="col-xs-8">
                                        <div class="message">
                                            <h4>Yüklemek istediğiniz dosyaları buraya sürükleyip bırakın</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="fallback">
                                    <input name="file" type="file" multiple />
                                </div>

                            </form>
                        </div>
                        <div class="col-md-12">
                            <button type="button" id="upload-button" class="btn btn-primary pull-right mt-3"><i class="fa fa-upload"></i>  Yükle</button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover table-responsive-sm table-responsive-md">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Görsel</th>
                                        <th>Durum</th>
                                        <th>Kapak</th>
                                        <th>Sil</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($images as $image)
                                        <tr id="tr-{{$image->id}}">
                                            <td>{{ $image->id }}</td>
                                            <td><img src="{{getProductImage($image->image)}}" style="width: 150px" alt="Ürün Resmi"></td>
                                            <td>
                                                <input type="checkbox"
                                                       name="is_active"
                                                       id="is_active"
                                                       class="is_active"
                                                       data-id="{{$image->id}}"
                                                       data-toggle="toggle"
                                                       data-on="Aktif"
                                                       data-off="Pasif"
                                                       data-onstyle="success"
                                                       data-offstyle="danger"
                                                       data-route="{{route("updateImageStatus")}}"
                                                       @if($image->is_active == 1) checked @endif
                                                >
                                            </td>
                                            <td>
                                                <input type="radio"
                                                       name="is_cover"
                                                       id="is_cover"
                                                       class="is_cover"
                                                       data-id="{{$image->id}}"
                                                       data-toggle="toggle"
                                                       data-on="Evet"
                                                       data-off="Hayır"
                                                       data-onstyle="success"
                                                       data-offstyle="danger"
                                                       data-route="{{route("product-images.setCover")}}"
                                                       @if($image->is_cover == 1) checked @endif
                                            </td>
                                            <td>
                                                <button class="btn btn-danger delete-button" data-id="{{$image->id}}" data-route="{{route("product-images.destroy", $image->id)}}" >Sil</button>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Ürün Görselleri</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{route("products.store")}}"
                                  method="POST"
                                  id="product-form"
                                  enctype="multipart/form-data">
                                @csrf
                                @method("POST")

                                <input type="hidden" name="is_active" value="1">
                                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Kategori</label>
                                            <select class="form-control select2 custom-select" id="category_id" name="category_id">

                                                <option value="">--- Seçim Yapın ---</option>
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                            @error("category_id")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">Başlık</label>
                                            <input type="text" class="form-control" name="title" placeholder="ÖRN: Satılık Telefon" value="{{old("title")}}">
                                            @error("title")
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="form-label">Slug (SEO URL için)</label>
                                                <input type="text" class="form-control" name="slug" placeholder="ÖRN: satilik-telefon" value="{{old("slug")}}">
                                                @error("slug")
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="form-label">Fiyat</label>
                                                <input type="text" class="form-control" name="price" placeholder="ÖRN: 250" value="{{old("price")}}">
                                                @error("price")
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Açıklama</label>
                                    <textarea name="description" class="form-control">{{old("description")}}</textarea>
                                    @error("description")
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Adres</label>
                                    <textarea name="address" class="form-control">{{old("address")}}</textarea>
                                    @error("address")
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>

                                <button class="btn btn-primary btn-lg mt-5 mb-5" type="submit">Kaydet</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>--}}
@endsection

@push("js")
    <!-- forn-wizard js-->
    <script src="{{panelAsset("plugins/forn-wizard/js/material-bootstrap-wizard.js")}}"></script>
    <script src="{{panelAsset("plugins/forn-wizard/js/jquery.validate.min.js")}}"></script>
    <script src="{{panelAsset("plugins/forn-wizard/js/jquery.bootstrap.js")}}"></script>
    <script src="{{panelAsset("plugins/bootstrap-wizard/jquery.bootstrap.wizard.js")}}"></script>
    <script src="{{panelAsset("plugins/bootstrap-wizard/wizard.js")}}"></script>
    <!--Select2 js -->
    <script src="{{panelAsset("plugins/select2/select2.full.min.js")}}"></script>
    <script src="{{panelAsset("js/select2.js")}}"></script>
    <script src="{{panelAsset("js/dropzone.js")}}"></script>
    <script src="{{panelAsset("CustomOperations/productDZUpload.js")}}"></script>
    <!-- CKE EDItor -->
    <script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>

    <!-- Sweet alert Plugin -->
    <script src="{{panelAsset("plugins/sweet-alert/jquery.sweet-modal.min.js")}}"></script>
    <script src="{{panelAsset("plugins/sweet-alert/sweetalert.min.js")}}"></script>
    <script src="{{panelAsset("js/sweet-alert.js")}}"></script>
    <!-- WYSIWYG Editor js -->
    <script src="{{panelAsset("plugins/wysiwyag/jquery.richtext.js")}}"></script>
    <script src="{{panelAsset("plugins/wysiwyag/richText1.js")}}"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- User CRUD Operations-->
    <script src="{{panelAsset("CustomOperations/productImageCrud.js")}}"></script>


@endpush
