<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "title" => "Öğrenciden Satılık IPhone 7 Plus",
            "slug"  => "ogrenciden-satilik-iphone-7-plus",
            "description" => "Daha üst model bir telefon alacağım için satılıktır. Takas yoktur.",
            "address"   => "14 Mayıs Mh. 4016 Sk. No:10/6 Selçuk-İzmir",
            "price" => 4000,
            "is_active" => 1,
            "user_id" => 1,
            "category_id" => 3
        ];

        DB::table("products")->insert($data);
    }
}
