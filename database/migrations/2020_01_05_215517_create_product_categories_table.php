<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("title");
            $table->string("slug");
            $table->text("description");
            $table->integer("rank")->default(0);
            $table->string("image")->nullable();
            $table->boolean("is_active")->default(0);
            $table->bigInteger("parent_id")->nullable()->unsigned();
            $table->timestamps();

            $table->foreign("parent_id")
                ->references("id")
                ->on("product_categories")
                ->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categories');
    }
}
