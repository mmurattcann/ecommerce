<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("top_title")->nullable();
            $table->string("title");
            $table->text("description");
            $table->string("image");
            $table->integer("rank");
            $table->integer("is_active");
            $table->integer("has_button")->default(0);
            $table->string("url")->nullable();
            $table->string("button_title")->nullable();
            $table->string("button_class")->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
